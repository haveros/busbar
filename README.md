# Projeto

BusBar - Serviço de Barramento

## Descrição do projeto

O projeto tem o objetivo de realizar consultas em determinada api (configurável pelo usuário) de acordo com um arquivo de entrada excel e salvar o resultado da consulta em um diretório também configurável.

### Pré Requisitos


* [.Net Core 5.0](https://dotnet.microsoft.com/en-us/download/dotnet/5.0)
* [Visual Studio 2019 ou superior](https://visualstudio.microsoft.com/pt-br/vs/)
* **Selecione a opção desenvolvimento para desktop com .NET em Carga de trabalho**

![image.png](./image.png)

## Como utilizar

● Forneça a url do endpoint que deseja consumir (por padrão, utilizaremos a api dog facts)

preencha os dados de entrada como no padrão da imagem a seguir:
![image-1.png](./image-1.png)

### Clique em processar.

na versão 1.0, o aplicativo irá ler todos os arquivos com extensão xlsx na pasta de entrada informada, e 
caso a solicitação seja processada com sucesso, um novo arquivo excel será gerado na pasta de saída cofigurada:

![image-3.png](./image-3.png)

## Versão

Versão 1.0

## Autor

* **Mateus Primo Cardoso**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
