﻿using System.Collections.Generic;

namespace BusBar.Core.Models
{
    public class CreateExcelModel
    {
        public string PathToSave { get; set; }
        public List<CreateExcelItemModel> Items { get; set; } = new ();
    }
}
