﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BusBar.Core.Models
{
    public class DogFactModel
    {
        [JsonProperty("fact")]
        public string Fact { get; set; }
    }
}