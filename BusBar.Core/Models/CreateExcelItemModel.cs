﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace BusBar.Core.Models
{
    public class CreateExcelItemModel
    {
        public string SheetName { get; set; }
        public JArray Items { get; set; }
    }
}
