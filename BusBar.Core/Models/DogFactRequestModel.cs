﻿namespace BusBar.Core.Models
{
    public class DogFactRequestModel
    {
        public string EndPoint { get; set; }
        public string InputPath { get; set; }
        public string OutPutPath { get; set; }
    }
}
