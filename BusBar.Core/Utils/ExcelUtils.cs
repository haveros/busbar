﻿using BusBar.Core.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

namespace BusBar.Core.Utils
{
    public static class ExcelUtils
    {
        public static List<string> ReadAllColumns(string path)
        {
            return ReadColumnsXls(path, 1);
        }

        private static List<string> ReadColumnsXls(string path, int? columnIndex)
        {
            DataTable dtTable = new DataTable();
            List<string> rowList = new List<string>();
            var jData = new JObject();

            ISheet sheet;
            using (var stream = new FileStream(path, FileMode.Open))
            {
                stream.Position = 0;
                XSSFWorkbook xssWorkbook = new XSSFWorkbook(stream);
                sheet = xssWorkbook.GetSheetAt(0);
                IRow headerRow = sheet.GetRow(0);
                int cellCount = columnIndex.HasValue ? columnIndex.Value : headerRow.LastCellNum;

                for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++)
                {
                    IRow row = sheet.GetRow(i);
                    if (row == null) continue;
                    if (row.Cells.All(d => d.CellType == CellType.Blank)) continue;

                    for (int j = row.FirstCellNum; j < cellCount; j++)
                    {
                        if (row.GetCell(j) != null)
                        {
                            if (!string.IsNullOrEmpty(row.GetCell(j).ToString()) && !string.IsNullOrWhiteSpace(row.GetCell(j).ToString()))
                            {
                                rowList.Add(row.GetCell(j).ToString());
                            }
                        }
                    }
                }
            }

            return rowList;
        }

        public static void CreateXlsxFile(string fileName, CreateExcelModel model)
        {

            using (var fs = new FileStream(fileName,
                FileMode.Create, FileAccess.Write))
            {
                IWorkbook workbook = new XSSFWorkbook();
                foreach (var item in model.Items)
                {
                    DataTable table = (DataTable)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(item.Items), (typeof(DataTable)));
                    ISheet excelSheet = workbook.CreateSheet(item.SheetName);
                    IRow row = excelSheet.CreateRow(0);

                    List<String> columns = new List<string>();
                    int columnIndex = 0;

                    foreach (DataColumn column in table.Columns)
                    {
                        columns.Add(column.ColumnName);
                        columnIndex++;
                    }


                    int rowIndex = 0;

                    foreach (DataRow dsrow in table.Rows)
                    {
                        row = excelSheet.CreateRow(rowIndex);
                        int cellIndex = 0;
                        foreach (String col in columns)
                        {
                            row.CreateCell(cellIndex).SetCellValue(dsrow[col].ToString());
                            cellIndex++;
                        }

                        rowIndex++;
                    }
                }

                workbook.Write(fs);
            }
        }
    }
}