﻿using BusBar.Core.Models;
using BusBar.Core.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace BusBar.Core.Services
{
    public static class DogFactService
    {
        private static string[] _allowedFiles = { ".xlsx" };

        public async static Task ProcesssFile(DogFactRequestModel model)
        {
            if (Directory.EnumerateFiles(model.InputPath, "*.*", SearchOption.TopDirectoryOnly)
            .Where(s => _allowedFiles.Any(x=> s.Contains(x))).Count() <= 0)
                throw new FileNotFoundException("Nenhum arquivo xlsx encontrado");

            foreach (var file in Directory.GetFiles(model.InputPath))
            {
                if (_allowedFiles.Any(x => file.Contains(x)))
                {
                    var excelData = ExcelUtils.ReadAllColumns(file);
                    var excelConfig = new CreateExcelModel()
                    {
                        PathToSave = model.OutPutPath
                    };

                    foreach (var item in excelData)
                    {
                        var facts = await ListDogFactsAsync(model.EndPoint, item);

                        excelConfig.Items.Add(new()
                        {
                            SheetName = item,
                            Items = JArray.FromObject(facts)
                        });
                    }

                    ExcelUtils.CreateXlsxFile($"{model.OutPutPath}/output-{Path.GetFileName(file)}", excelConfig);
                }
            }
        }

        private async static Task<List<DogFactModel>> ListDogFactsAsync(string path, string number)
        {
            using (var client = new HttpClient())
            {
                var response = await client.GetAsync($"{path}?number={number}");

                if (!response.IsSuccessStatusCode)
                    throw new HttpRequestException("Erro ao consultar o endpoint");

                try
                {
                    return JsonConvert.DeserializeObject<List<DogFactModel>>(await response.Content.ReadAsStringAsync());
                }
                catch (Exception)
                {
                    throw new JsonException($"Erro ao deserializar a consulta ao dog facts :\n {await response.Content.ReadAsStringAsync()}");
                }
            }
        }
    }
}
