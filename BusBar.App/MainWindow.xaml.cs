﻿using BusBar.Core.Models;
using BusBar.Core.Services;
using Microsoft.Win32;
using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using MessageBox = System.Windows.MessageBox;

namespace BusBar.App
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private async void Process_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                loading.Visibility = Visibility.Visible;

                var model = new DogFactRequestModel()
                {
                    EndPoint = endpoint.Text,
                    InputPath = input.Text,
                    OutPutPath = output.Text
                };

                if (!Validate(model))
                    return;

                await DogFactService.ProcesssFile(model);

                MessageBox.Show("Arquivo de saída gerado com Sucesso");

                ClearInputs();
            }
            catch (Exception ex)
            {
                PrintError(ex.Message);
            }
            finally
            {
                loading.Visibility = Visibility.Hidden;
            }
        }

        private void ClearInputs()
        {
            input.Text = String.Empty;
            output.Text = String.Empty;
        }

        private void PrintError(string msg)
        {
            MessageBox.Show(msg, $"{string.Concat(Enumerable.Repeat("-", 10))} ERRO {string.Concat(Enumerable.Repeat("-", 10))}");
        }

        private bool Validate(DogFactRequestModel model)
        {
            if (!Uri.IsWellFormedUriString(model.EndPoint, UriKind.Absolute))
            {
                PrintError("Url de endpoint inválida!");
                return false;
            }

            if (!Directory.Exists(model.InputPath))
            {
                PrintError("Caminho de entrada não encontrado!");
                return false;
            }

            if (!Directory.Exists(model.OutPutPath))
            {
                PrintError("Caminho de saída não encontrado!");
                return false;
            }

            return true;
        }

        private void searchInput_Click(object sender, RoutedEventArgs e)
        {
            input.Text = SelectFolder();
        }

        private void searchOutput_Click(object sender, RoutedEventArgs e)
        {
            output.Text = SelectFolder();
        }

        private string SelectFolder()
        {
            FolderBrowserDialog openFolder = new FolderBrowserDialog();
            openFolder.ShowDialog();
            return  openFolder.SelectedPath;
        }
    }
}
